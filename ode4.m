function [output_time, output_vector_1, output_vector_2, lowerBoundError] = ode4(testFunction,initialTime,initialConditions,integrationInterval,maxLowerBoundError)
	if nargin<5 || isempty(maxLowerBoundError)
		maxLowerBoundError = 1e0;
	end

	y1(1,:) = initialConditions;
	y2(1,:) = initialConditions;
	t(1,1) = initialTime;
	h = integrationInterval;
	hm = h./2;

	i = 1;
	while 1

		k1(1,:) = h.*testFunction(t(i,1),y1(i,:));
		k1(2,:) = h.*testFunction((t(i,1)+hm),(y1(i,:)+(k1(1,:)./2)));
		k1(3,:) = h.*testFunction((t(i,1)+hm),(y1(i,:)+(k1(2,:)./2)));
		k1(4,:) = h.*testFunction((t(i,1)+h),(y1(i,:)+k1(3,:)));
		y1(i+1,:) = y1(i,:) + (k1(1,:) + 2.*(k1(2,:) + k1(3,:)) + k1(4,:))./6;

		k2(1,:) = h.*testFunction(t(i,1),y2(i,:));
		k2(2,:) = h.*testFunction((t(i,1)+hm),(y2(i,:)+(k2(1,:)./2)));
		k2(3,:) = h.*testFunction((t(i,1)+hm),(y2(i,:)+(k2(2,:)./2)));
		k2(4,:) = h.*testFunction((t(i,1)+h),(y2(i,:)+k2(3,:)));
		y2(i+1,:) = y2(i,:) + k2(1,:)./6 + 2.*k2(2,:)./6 + k2(3,:)./3 + k2(4,:)./6;

		t(i+1,1) = t(i,1) + h;

		i = i + 1;

		% lbe(i-1,1) = abs(y1(i,1) - y2(i,1))./abs(y1(i,1));
		
		lbe(i-1,1) = abs((y1(i,1) - y2(i,1))./(y1(i,1) + y2(i,1)));

		if lbe(i-1,1) > maxLowerBoundError
		fprintf('ODE4, Iteracao de parada:%d\n',i);
			break;
		end
	end

	output_time = t;
	output_vector_1 = y1;
	output_vector_2 = y2;
	lowerBoundError = lbe;
end
