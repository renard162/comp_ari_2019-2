function [output_time, output_vector_1, output_vector_2, lowerBoundError] = ode1(testFunction,initialTime,initialConditions,integrationInterval,maxLowerBoundError)
	if nargin<5 || isempty(maxLowerBoundError)
		maxLowerBoundError = 1e0;
	end

	y1(1,:) = initialConditions;
	y2(1,:) = initialConditions;
	t(1,1) = initialTime;
	h = integrationInterval;
	
	i = 1;
	while 1
		k1(1,:) = h.*testFunction(t(i,1),y1(i,:));
		y1(i+1,:) = y1(i,:) + k1(1,:);

		k2(1,:) = h.*testFunction(t(i,1),y2(i,:));
		y2(i+1,:) = ((4.*y2(i,:) + 2.*k2(1,:))./2) - y2(i,:);

		t(i+1,1) = t(i,1) + h;

		i = i + 1;

		% lbe(i-1,1) = abs(y1(i,1) - y2(i,1))./abs(y1(i,1));
		
		lbe(i-1,1) = abs((y1(i,1) - y2(i,1))./(y1(i,1) + y2(i,1)));

		if lbe(i-1,1) > maxLowerBoundError
			fprintf('ODE1, Iteracao de parada:%d\n',i);
			break;
		end
	end

	output_time = t;
	output_vector_1 = y1;
	output_vector_2 = y2;
	lowerBoundError = lbe;
end
