clear;
close all;
clc;

%%
% Definicoes iniciais
lbe_max = 0.5e-12; %Maximo "lower bound error" percentual aceitável

% Equacao diferencial
m = 1; %kg
k = 1; %N*m
sig = 0; %amortecimento
fy = @(t,y) [ y(2); -(k./m).*y(1)]; %-(sig./m).*y(2)]; <- Parcela do amortecimento, retirada para benchmark

y0 = [10, 0]; %Condicoes iniciais
h = 1e-1; %Passo de integracao
t = 0; %Tempo inicial

[tf1, y1a1, y1a2, lbe1] = ode1(fy,t,y0,h,lbe_max);
[tf2, y2a1, y2a2, lbe2] = ode2(fy,t,y0,h,lbe_max);
[tf3, y3a1, y3a2, lbe3] = ode3(fy,t,y0,h,lbe_max);
[tf4, y4a1, y4a2, lbe4] = ode4(fy,t,y0,h,lbe_max);
% [tf5, y5a1, y5a2, lbe5] = ode5(fy,t,y0,h,lbe_max); %Este método possui necessariamente passo de integração variável, inviabilizando essa comparação


figure(1);
plot(y1a1(:,1),'b');
hold on;
plot(y1a2(:,1),'r--');
figure(11);
plot(lbe1(:,1));

figure(2);
plot(y2a1(:,1),'b');
hold on;
plot(y2a2(:,1),'r--');
figure(12);
plot(lbe2(:,1));

figure(3);
plot(y3a1(:,1),'b');
hold on;
plot(y3a2(:,1),'r--');
figure(13);
plot(lbe3(:,1));

figure(4);
plot(y4a1(:,1),'b');
hold on;
plot(y4a2(:,1),'r--');
figure(14);
plot(lbe4(:,1));

% Passo de integração variável
% figure(5);
% plot(y5a1(:,1),'b');
% hold on;
% plot(y5a2(:,1),'r--');
% figure(15);
% plot(lbe5(:,1));

