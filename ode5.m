% ERRADO !!!! desprezar
function [output_time, output_vector_1, output_vector_2, lowerBoundError] = ode4(testFunction,initialTime,initialConditions,integrationInterval,maxLowerBoundError)
	if nargin<5 || isempty(maxLowerBoundError)
		maxLowerBoundError = 1e0;
	end

	y1(1,:) = initialConditions;
	y2(1,:) = initialConditions;
	t(1,1) = initialTime;
	h = integrationInterval;

	a2=1/4; b2=1/4;
	a3=3/8; b3=3/32; c3=9/32;
	a4=12/13; b4=1932/2197; c4=-7200/2197; d4=7296/2197;
	a5=1; b5=439/216; c5=-8; d5=3680/513; e5=-845/4104;
	a6=1/2; b6=-8/27; c6=2; d6=-3544/2565; e6=1859/4104; f6=-11/40;

	% n1a=25/216; n3a=1408/2565; n4a=2197/4104; n5a=-1/5; n6a=0; %RKF45 1a orbita
	n1a=16/135; n3a=6656/12.825; n4a=28.561/56.430; n5a=-9/50; n6a=2/55; %RKF55 1a orbita
	%ng=1; n13g=25; n56g=1; n1b=1/216; n3b=56.32/2565; n4b=2197/4104; n5b=-1/5; n6b=0; %RKF45 2a orbita
	ng=2; n13g=8; n56g=1; n1b=1/135; n3b=416/12.825; n4b=28.561/112.86; n5b=-4.5/50; n6b=1/55; %RKF55 2a orbita

	i = 1;
	while 1

		k1(1,:) = h.*testFunction(t(i,1) ,y1(i,:));
		k1(2,:) = h.*testFunction(t(i,1)+a2.*h ,y1(i,:)+b2.*k1(1,:));
		k1(3,:) = h.*testFunction(t(i,1)+a3.*h ,y1(i,:)+b3.*k1(1,:)+c3.*k1(2,:));
		k1(4,:) = h.*testFunction(t(i,1)+a4.*h ,y1(i,:)+b4.*k1(1,:)+c4.*k1(2,:)+d4.*k1(3,:));
		k1(5,:) = h.*testFunction(t(i,1)+a5.*h ,y1(i,:)+b5.*k1(1,:)+c5.*k1(2,:)+d5.*k1(3,:)+e5.*k1(4,:));
		k1(6,:) = h.*testFunction(t(i,1)+a6.*h ,y1(i,:)+b6.*k1(1,:)+c6.*k1(2,:)+d6.*k1(3,:)+e6.*k1(4,:)+f6.*k1(5,:));
		y1(i+1,:) = y1(i,:) + n1a.*k1(1,:) + n3a.*k1(3,:) + n4a.*k1(4,:) + n5a.*k1(5,:) + n6a.*k1(6,:);

		k2(1,:) = h.*testFunction(t(i,1) ,y2(i,:));
		k2(2,:) = h.*testFunction(t(i,1)+a2.*h ,y2(i,:)+b2.*k2(1,:));
		k2(3,:) = h.*testFunction(t(i,1)+a3.*h ,y2(i,:)+b3.*k2(1,:)+c3.*k2(2,:));
		k2(4,:) = h.*testFunction(t(i,1)+a4.*h ,y2(i,:)+b4.*k2(1,:)+c4.*k2(2,:)+d4.*k2(3,:));
		k2(5,:) = h.*testFunction(t(i,1)+a5.*h ,y2(i,:)+b5.*k2(1,:)+c5.*k2(2,:)+d5.*k2(3,:)+e5.*k2(4,:));
		k2(6,:) = h.*testFunction(t(i,1)+a6.*h ,y2(i,:)+b6.*k2(1,:)+c6.*k2(2,:)+d6.*k2(3,:)+e6.*k2(4,:)+f6.*k2(5,:));
		y2(i+1,:) = y2(i,:) + ng.*(n13g.*(n1b.*k2(1,:) + n3b.*k2(3,:)) + n4b.*k2(4,:) + n56g.*(n5b.*k2(5,:) + n6b.*k2(6,:)));

		t(i+1,1) = t(i,1) + h;

		i = i + 1;

		% lbe(i-1,1) = abs(y1(i,1) - y2(i,1))./abs(y1(i,1));

		lbe(i-1,1) = abs((y1(i,1) - y2(i,1))./(y1(i,1) + y2(i,1)));

		if lbe(i-1,1) > maxLowerBoundError || i >= 50e3
			fprintf('ODE5, Iteracao:%d\n',i);
			break;
		end
	end

	output_time = t;
	output_vector_1 = y1;
	output_vector_2 = y2;
	lowerBoundError = lbe;
end
